1 报错 “routeDone with a webviewId 1 that is not the current page”

原因 微信小程序出现路由问题，可能是实际页面文件和app.json代码不匹配导致的，

解决 查看page文件中的页面是否都在json中有相应的配置，刷新即可

或者在微信开发工具中把基础库版本改为2.27.2以前的版本，就不会报这个错

2 使用微信开发者工具不能生成完整项目结构只生成project.config.json

原因 设置通用工作区的地址与项目存储地址不一致  

解决 修改为同一地址即可

3 Unhandled promise rejection RangeError: WebAssembly.Memory(): could not allocate memory

未处理的 Promise 边缘错误 无法分配内存

原因 版本过高

解决 换一个版本如2.27.1 重新编译即可

4 Error: module 'pages/index/index.js' is not defined, require args is 'pages/index/index.js'

函数后面少了个 ‘ , ’  添加逗号即可

5 ReferenceError: regeneratorRuntime is not defined

解决 微信开发者工具 本地设置开启增强编译即可

6 routeDone with a webviewId 7 that is not the current page

页面文件顺序和app.json顺序不一致 pages从上到下顺序对应

解决 修改app.json下顺序即可

7 Response timeout while trying to fetch https://registry.npmmirror.com/@typescript-eslint%2fparser (over 30000ms)

npm安装依赖报错 访问外网库限制 使用国内镜像
8 TypeError: Cannot read property 'join' of null
一般接口路径引入错误 修改正确引入

