##### Flex布局

```css
flex-direction: row //默认 主轴水平方向 起点在左边

//column 垂直方向 起点在上面

//row-reverse      column-reverse
```

教程

[Flex 布局语法教程 | 菜鸟教程 (runoob.com)](https://www.runoob.com/w3cnote/flex-grammar.html)

物理像素

css像素 px

//px rem em 

dpr 设备像素比  物理像素/设备独立像素

IPhone6 dpr = 2

##### 移动端适配

###### viewport适配 与rem适配

viewport适配

```html
<meta name="viewport" content="width=device-width,initial-scale=1.0">
```

rem适配

root em

```js
  function remRefresh() {
            let clientWidth = document.documentElement.clientWidth;
            //将屏幕等分10份
            document.body.style.fontSize = '12px';
        }
        window.addEventListener('pageshow', () => {
                remRefresh()
            })
            // 函数防抖
            // 防抖和节流 两者都可以理解为延时执行，防抖是一段时间内不断的触发只执行最后一次，
            // 节流是一段时间内不断触发，会均匀的间隔执行。
        let timeoutId
        window.addEventListener('resize', () => {
            timeoutId && clearTimeout(timeoutId)
            timeoutId = setTimeout(() => {
                remRefresh()
            }, 300)
        })

```

Js 模块化开发 具备特定功能的Js文件 
小程序 组件化开发 具备特定功能效果的代码集合

小程序 没有DOM 组件化开发 适配单位rpx

Iphone 6   1rpx = 0.5px

##### 数据绑定

```js
  data: {
     msg:'Hello Tomcat'
  },
```

```html
<text>{{msg}}</text>
```

AppData调试器

修改数据 this.setData

```js
 onLoad: function (options) {
    // 修改msg状态数据 this.setData
    // console.log(this.data.msg)
    this.setData({
      msg:'Jerry'
    })
    // console.log(this.data.msg)
  }
```

##### 数据劫持代理

```js
//vue数据劫持代理
let data = {
  username:'curry',
  age:18
}
//模拟组件实例
let _this = {
}
//利用Object.defineProperty()
for(let item in data){
  console.log(item,data[item])
  Object.defineProperty(_this,item,{
    //用来获取扩展属性值 获取该属性值时候调用get方法
      get(){
          return data[item]
      },
      // 监视扩展属性 一修改就调用
      set(newValue){
        // 不要在set方法中直接修改扩展属性_this值  迂回修改data
        data[item] = newValue
      }
  })
}
//console.log(_this)
// 通过Object.defineProperty的get()方法添加的扩展属性不能直接key.value修改
_this.username = 'wade'
console.log(_this.username)
```

##### 事件绑定

冒泡事件 非冒泡事件

bind绑定 不会阻止事件冒泡   catch绑定会阻止事件向上冒泡

```js
<view bindtap = "handleTap"></view>
<view catchtap = "handleTap"></view>
```

冒泡

事件流三个阶段  捕获(从外向内)  执行  冒泡(从内向外)

##### 路由跳转

```javascript
onReady: function () {
	wx.switchTab
	wx.reLaunch
	wx.redirectTo
	wx.navigateTo
},
```

##### 生命周期

##### 小程序

​	swiper  scroll-view

webstorm 输入image 按tab键才会补全代码

项目采用网易云音乐NeteaseCloudMusicApi 本地库发送请求



```css
/*单行文本溢出隐藏*/
display: block;
white-space: nowrap;
over-flow: hidden;  /*over-flow:hidden作用于块级元素才生效*/
text-overflow: ellipsis;

	 /* 多行文本溢出隐藏 */
overflow: hidden;
text-overflow: ellipsis;
display: -webkit-box;
-webkit-box-orient: vertical;/* 设置对齐模式 */
-webkit-line-clamp:2 /* 设置多行行数 */
```

##### 前后端交互

NodeJs中间层 分发路由 减轻主服务器压力

wx.request(Object object)

小程序接口协议必须是https协议

测试使用http协议 可以在本地设置 勾选不校验合法域名

##### 封装请求功能函数

wx:key用下标index做值 新增数据时 会导致插入后的数据index变化 导致key不是唯一不变标识

##### 自定义组件

scroll-view计算高度默认为纵向 避免样式错误一般需要手动设置scroll-view高度

##### 登陆流程

1 收集表单项数据

2 前端验证

​	2.1 验证用户信息（账号，密码）是否合法

​	2.2 前端验证不通过提示用户 不需发请求到后端

​	2.3 前端验证通过 发请求（携带账号 密码等参数）给服务器端

3 后端验证

3.1验证用户是否存在 验证密码是否正确 验证正确返回前端数据

3.2用户不存在 直接返回 告诉用户不存在

input 输入框内容实时改变就触发	change输入框内容改变且失去焦点才触发

##### 事件委托

1 什么是事件委托

将子元素事件委托（绑定）给父元素身上

2 好处

减少绑定次数 后期新添加元素也可以享用之前委托事件

3 原理

事件冒泡

4 触发事件委托的对象是谁

触发事件的是子元素

5 如何找到对应触发的子元素

event.target

6 currentTarget && target

currentTarget 要求绑定事件元素一定是触发事件元素 （绑定对象）

target 绑定事件元素不一定是触发事件元素 （点击对象） 

##### 数据缓存

wx.setStorageSync || wx.setStorage

存储数据一般转为JSON数据 

```javascript
// 将用户信息存储至本地
wx.setStorageSync('userInfo', JSON.stringify(result.profile));
```

取出时再编译为JS对象

```javascript
userInfo: JSON.parse(userInfo)
```



小程序本地缓存单个 key 允许存储的最大数据长度为 1MB，所有数据存储上限为 10MB

属于永久存储 和localStorage类似

不要在生命周期函数使用async和await函数

位移运算符

```js
>>>0 右移零位可以将非number数据强制转化成number
```



本地接口数据库 NeteaseCloudMusicApi-master    硅谷音乐_server

##### 背景音频

```javascript
 // 控制音乐播放 || 暂停的功能函数
    async musicControl(isPlay, musicId) {
        let backAudioManager = wx.getBackgroundAudioManager();
        if (isPlay) {
            // 音乐播放
            // properties(Read only)(duration,currentTime,paused,buffered)
            // properties(src(m4a, aac, mp3, wav),startTime,title,epname,singer,coverImgUrl,webUrl,protocol)
            // 获取音乐播放连接
            let musicLinkData = await request('/song/url', { id: musicId })
            let musicLink = musicLinkData.data[0].url
            // 音乐链接
            backAudioManager.src = musicLink
            backAudioManager.title = this.song.data.name
        } else {
            //音乐暂停
            backAudioManager.pause()
        }
    },
```

##### 跨页面通信 引入NPM

npm init -y

下载pubsub  消息订阅与发布 npm install pubsub-js

自定义事件

先订阅后发布

绑定事件  

​	Pubsub.subscribe(事件名，事件回调) 订阅方  接收数据

触发事件

​	Pubsub.publish(事件名，数据) 发布方  提供数据

absolute 如果没有父元素relative 会以最近开启定位absolute的元素作为父元素

##### 时间格式化

momentjs  npm install moment

```javascript
 let durationTime = moment(songData.songs[0].dt).format('mm:ss')
```

##### 函数防抖

##### 模版使用

模版动态注入数据

小程序获取用户唯一标识openID

code加密解密

##### 分包加载

常规分包 

app .json subpackages

独立分包 

设置independent 为true  不依赖主包内容

分包预下载

##### 小程序支付

https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=7_3&index=1