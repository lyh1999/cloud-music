// pages/songDetail/songDetail.js
import request from '../../../utils/request'
import PubSub from 'pubsub-js'
import moment from 'moment'
// 获取全局实例数据
const appInstance = getApp();
Page({
    /**
     * 页面的初始数据
     */
    data: {
        isPlay: false, //音乐是否播放
        song: {}, //歌曲详情对象
        musicId: '', //音乐ID
        musicLink: '', //音乐链接
        currentTime: '00:00', //当前时长
        durationTime: '00:00', //总时长
        currentWidth:0 //实时进度条宽度
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        // console.log(options.song)
        // options接收路由跳转的query参数
        // 原生小程序路由传参对参数长度有限制 参数长度过长 会自动截取掉
        let musicId = options.musicId
        this.setData({
            musicId
        })
        this.getMusicInfo(musicId);
        /* 
            如果用户操作系统控制音乐播放/暂停，页面监听不到 页面显示与是否播放状态和真实音乐播放状态不一致
            解决方案 通过控制音频实例 backAudioManager 监听音乐播放||暂停  将backAudioManager添加到this上解决跨作用域问题
        */
        // 监听音乐播放 || 暂停 || 停止
        // 判断当前音乐是否在播放
        if (appInstance.globalData.isMusicPlay && appInstance.globalData.musicId === musicId) {
            // 修改当前页面音乐播放状态为true
            this.setData({
                isPlay: true
            })
        }
        this.backgroundAudioManager = wx.getBackgroundAudioManager();
        this.backgroundAudioManager.onPlay(() => {
            // 修改音乐状态为播放
            this.changePlayState(true)
                // 修改全局音乐播放状态
                // appInstance.globalData.isMusicPlay = true
            appInstance.globalData.musicId = musicId
        })
        this.backgroundAudioManager.onPause(() => {
            this.changePlayState(false)
                // 修改全局音乐播放状态
                // appInstance.globalData.isMusicPlay = false
        })
        this.backgroundAudioManager.onStop(() => {
            this.changePlayState(false)
                // 修改全局音乐播放状态
        })
        // 监听音乐实时播放进度
        this.backgroundAudioManager.onTimeUpdate(()=>{
            // console.log('总时长',this.backgroundAudioManager.duration)
            // console.log('实时时长',this.backgroundAudioManager.currentTime)
            let currentTime = moment(this.backgroundAudioManager.currentTime*1000).format('mm:ss')
            let currentWidth = this.backgroundAudioManager.currentTime/this.getBackgroundAudioManager.duration*450
            this.setData({
                currentTime,
                currentWidth
            })
        })
        // 监听音乐播放自然结束
        this.backgroundAudioManager.onEnded(()=>{
            // 自动切换至下一首音乐 并且自动播放
            PubSub.publish('switchType','next')
            // 将实时进度条长度还原至0
            this.setData({
                currentWidth: 0,
                currentTime: '00:00'
            })
        })
    },
    // 封装修改状态功能函数
    changePlayState(isPlay) {
        this.setData({
            isPlay
        })
        appInstance.globalData.isMusicPlay = isPlay
    },
    // 获取音乐详情功能函数
    async getMusicInfo(musicId) {
        let songData = await request('/song/detail', { ids: musicId })
        let durationTime = moment(songData.songs[0].dt).format('mm:ss')
        this.setData({
                song: songData.songs[0],
                durationTime
                // songData.songs[0].dt  当前音乐时长ms
            })
            // 动态修改窗口标题
        wx.setNavigationBarTitle({
            title: this.data.song.name,
        });
    },
    // 点击播放或者暂停回调函数
    handleMusicPlay() {
        let isPlay = !this.data.isPlay
        let { musicId, musicLink } = this.data
            // 修改视图播放状态
            // this.setData({
            //     isPlay
            // })
        this.musicControl(isPlay, musicId, musicLink)
    },
    // 控制音乐播放 || 暂停的功能函数
    async musicControl(isPlay, musicId, musicLink) {
        if (isPlay) {
            // 音乐播放
            // properties(Read only)(duration,currentTime,paused,buffered)
            // properties(src(m4a, aac, mp3, wav),startTime,title,epname,singer,coverImgUrl,webUrl,protocol)
            // 获取音乐播放连接
            if (!musicLink) {
                // 如果没有链接 重新获取链接 已存有链接 直接使用
                let musicLinkData = await request('/song/url', { id: musicId })
                musicLink = musicLinkData.data[0].url
                this.setData({
                    musicLink
                })
            }
            // 音乐链接
            backgroundAudioManager.src = musicLink
            backgroundAudioManager.title = this.song.data.name
        } else {
            //音乐暂停
            backgroundAudioManager.pause()
        }
    },
    // 点击切换歌曲回调  用ID作为标识
    handleSwitch(event) {
        let type = event.currentTarget.id;
        // 先关闭当前播放音乐
        this.backgroundAudioManager.stop()
            // 发布消息数据给recommendSong页面 发布方  发布请求类型Type
            // 订阅来自recommend页面发布的ID消息
        PubSub.subscribe('musicId', (msg, musicId) => {
            // console.log(musicId)
            // 获取音乐详情信息
            this.getMusicInfo(musicId)
                // 点击下一首时 同时自动播放音乐
            this.musicControl(true, musicId)
                // 取消订阅
            PubSub.unsubscribe('musicId')
        })
        PubSub.publish('switchType', type)
            // 切歌 跨页面通信
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})