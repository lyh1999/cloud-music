// 发送ajax请求
// 1 封装功能函数  
// 功能点明确
// 函数内部保留固定代码
// 将动态参数抽取为形参
// 设置形参默认值
// 2 封装功能组件 
// 功能点明确
// 函数内部保留固定代码
// 将动态参数抽取成props参数
// 设置组件必要性及数据类型
import config from './config'
export default (url, data = {}, method = 'GET') => {
    // 异步任务 非阻塞
    return new Promise((resolve, reject) => {
        // 初始化 Promise实例状态为pending
        wx.request({
            url: config.host + url,
            data,
            method,
            // 设置请求头携带cookie
            header: {
                cookie: wx.setStorageSync('cookies') ? wx.wx.getStorageSync('cookies').find(item => item.indexOf('MUSIC_U') !== -1) : ''
            },
            success: (res) => {
                // console.log('请求成功', res)
                // 修改promise状态为resolve
                if (data.isLogin) {
                    // 登陆请求 如果请求路由带有isLogin 将用户从cookie存入本地  
                    wx.setStorage({
                        key: 'cookies',
                        data: res.cookies
                    });
                }
                resolve(res.data)
            },
            fail: (err) => {
                // console.log('请求失败', err)
                // 修改promise状态为reject 并下放err
                reject(err)
            }
        })
    })
}