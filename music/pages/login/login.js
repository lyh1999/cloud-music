// pages/login/login.js
import request from '../../utils/request'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        phone: "",
        password: ""
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {

    },
    // 表单内容发生改变回调
    handleInput(event) {
        // console.log(event.detail.value)
        // let type = event.currentTarget.id;  //phone|| password id校验 唯一值
        let type = event.currentTarget.dataset.type; //data-xx 自定义校验 可以多值校验
        this.setData({
            // 匹配到对应Type值 然后赋值给detail.value
            [type]: event.detail.value
        })
    },
    // 登陆的回调
    async login() {
        let { phone, password } = this.data
            // 前端验证
            // 手机号验证
            // 内容为空 格式不正确 格式正确
        if (!phone) {
            // 有成功失败回调异步任务 结束return
            wx.showToast({
                title: '手机号不能为空',
                icon: 'none'
            });
            return;
        }
        // 定义正则表达式
        let phoneReg = /^1[3-9]\d{9}$/
        if (!phoneReg.test(phone)) {
            wx.showToast({
                title: '手机号格式错误',
                icon: 'none'
            });
            return;
        }
        if (!password) {
            wx.showToast({
                title: '密码不能为空',
                icon: 'none'
            });
            return;
        }

        // wx.showToast({
        //     title: '前端验证通过',
        //     // icon: 'success'
        // });
        // return;
        // 后端验证
        let result = await request('/login/cellphone', { phone, password, isLogin: true });
        if (result.code == 200) {
            wx.showToast({
                title: '登陆成功',
                // icon: 'success'
            });
            // 将用户信息存储至本地
            wx.setStorageSync('userInfo', JSON.stringify(result.profile));
            // 跳转至个人中心
            wx.reLaunch({
                url: 'pages/personal/personal',
            });
        } else if (result.code == 400) {
            wx.showToast({
                title: '手机号错误',
                icon: 'none'
            });
        } else if (result.code == 502) {
            wx.showToast({
                title: '密码错误',
                icon: 'none'
            });
        } else {
            wx.showToast({
                title: '登陆失败 请重新登陆',
                icon: 'none'
            });
        }

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})