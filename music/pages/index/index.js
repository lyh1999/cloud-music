// pages/index/index.js
import request from '../../utils/request'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        bannerList: [], //轮播图数据
        recommendList: [], //推荐歌单数据
        topList:[], //排行榜数据
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: async function(options) {
        // wx.request({
        //     url: 'http://localhost:3000/banner',
        //     data: {
        //         type: 2
        //     },
        //     success: (res) => {
        //         console.log('请求成功', res)
        //     },
        //     fail: (err) => {
        //         console.log('请求失败', err)
        //     }
        // })
        let bannerListData = await request('/banner', { type: 2 })
        let recommendListData = await request('/personalized', { limit: 10 });
        // 需要根据idx值获取对应数据 idx 0-20  需要0-4  发送5次请求
        let index = 0;
        let resultArr = [];
        while(index < 5){
            let topListData = await request('/top/list',{idx:index++})
            let topListItem = {
                name:topListData.playlist.name,
                // 截取数组 splice 会修改原数组 可以对指定数组进行增删改 slice 不会修改原数组 slice（0,3）包含截取位置 不含结束位置
                tracks:topListData.playlist.tracks.slice(0,3)
            }
            resultArr.push(topListItem)
            // 此处更新 减少页面白屏时间 渲染次数会增加
            this.setData({
                topList:resultArr
            })
        }
        this.setData({
                bannerList: bannerListData.banners,
                // 获取推荐歌单数据
                recommendList: recommendListData.result,
                // 获取排行榜数据 更新topList状态值 此处导致更新长时间白屏
                // topList: resultArr
            })
            // console.log(result)
            // 获取推荐歌单数据
            // let recommendListData = await request('/personalized', {limit: 10});
            // this.setData({
            //     recommendList:recommendListData.result
            // })
    },
    toRecommendSong(){
        wx.navigateTo({
          url: '/songPackage/pages/recommendSong/recommendSong',
        })
    },
    toOther(){
        wx.navigateTo({
          url: '/otherPackage/pages/other/other',
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})