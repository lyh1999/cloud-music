// pages/search/search.js
import request from '../../utils/request'
let isSend = false //函数节流使用
Page({

    /**
     * 页面的初始数据
     */
    data: {
        placeholderContent: '', //placeholder默认内容
        hotList: [], //热搜榜数据
        searchContent: '', //用户输入表单项数据
        searchList: [], //关键字模糊匹配的数据
        historyList: [], //搜索历史记录
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        // 获取初始化数据
        this.getInitData()
            // 获取历史记录
        this.getSearchHistory()
    },
    // 初始化数据
    async getInitData() {
        let placeholderData = await request('/search/default')
        let hotListData = await request('/search/hot/detail')
        this.setData({
            placeholderContent: placeholderData.data.showKeyword,
            hotList: hotListData.data
        })
    },
    // 表单项内容发生改变回调
    handleInputChange(event) {
        // console.log(event)
        // 发请求获取关键字模糊匹配数据
        this.setData({
                searchContent: event.detail.value.trim()
            })
            // 函数节流
        if (isSend) {
            return //第一次进来值为false  第二次进入为true 跳过 不开启定时器
        }
        this.isSend = true //isSend置为true
        this.getSearchList()
        setTimeout(() => {
                // 开启定时器发送请求
                isSend = false //异步执行 等.3s后 重新置为false 发送请求  .3s之内值都是true
            }, 300)
            // 函数防抖
    },
    // 获取数据功能函数
    async getSearchList() {
        if (!this.data.searchContent) {
            this.setData({
                searchList: []
            })
            return
        }
        let { historyList, searchContent } = this.data
        let searchListData = await request('/search', {
            keywords: searchContent,
            limit: 10
        })
        this.setData({
                searchList: searchListData.result.songs
            })
            // 将搜索关键字添加到搜索历史记录
        if (historyList.indexOf(searchContent) !== -1) {
            //将重复内容提前
            historyList.splice(historyList.indexOf(searchContent), 1) //删除重复内容
        }
        historyList.unshift(searchContent); //往前添加
        this.setData({
                historyList
            })
            // 将数据历史记录存储在本地
        wx.setStorageSync('searchHistory', historyList);
    },
    // 获取本地历史记录功能函数
    getSearchHistory() {
        let historyList = wx.getStorageSync('searchHistory');
        if (historyList) {
            this.setData({
                historyList
            })
        }
    },
    // 清空搜素内容
    clearSearchnContet() {
        this.setData({
            searchContent: '',
            searchList: '',
        })
    },
    // 删除历史记录
    deleteSearchHistory() {
        wx.showModal({
            content: '确认删除吗?',
            success: (res) => {
                // console.log(res)
                if (res.confirm) {
                    // 清空data中historyList
                    this.setData({
                            historyList: []
                        })
                        // 清空本地存储历史记录
                    wx.removeStorageSync('searchHistory');
                }
            }
        });
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})