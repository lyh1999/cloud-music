// pages/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
     msg:'Hello Tomcat'
  },
  handleParent(){
    console.log('parent')
  },
  handleChild(){
    console.log('child')
  },
  toLogs(){
    wx.navigateTo({
      url: '/pages/logs/logs',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 修改msg状态数据 this.setData
    // console.log(this.data.msg)
    // this.setData({
      // msg:'Jerry'
    // })
    // console.log(this.data.msg)
    console.log('onload');
    // debugger;
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    console.log('onready');
// //vue数据劫持代理
// let data = {
//   username:'curry',
//   age:18
// }
// //模拟组件实例
// let _this = {
// }
// //利用Object.defineProperty()
// for(let item in data){
//   // console.log(item,data[item])
//   Object.defineProperty(_this,item,{
//     //用来获取扩展属性值 获取该属性值时候调用get方法
//       get(){
//           return data[item]
//       },
//       // 监视扩展属性 一修改就调用
//       set(newValue){
//         // 不要在set方法中直接修改扩展属性_this值  迂回修改data
//         data[item] = newValue
//       }
//   })
// }
// console.log(_this)
// // 通过Object.defineProperty的get()方法添加的扩展属性不能直接key.value修改
// _this.username = 'wade'
// console.log(_this.username)
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})